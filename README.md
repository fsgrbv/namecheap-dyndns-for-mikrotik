# README #

Script for mikrotik RouterOS to update DynDNS record on namecheap.com

### Tested with versions of RouterOS: ###

* 6.18

### Used sources: ###

* [https://www.namecheap.com/support/knowledgebase/article.aspx/29/11/how-do-i-use-the-browser-to-dynamically-update-hosts-ip](https://www.namecheap.com/support/knowledgebase/article.aspx/29/11/how-do-i-use-the-browser-to-dynamically-update-hosts-ip)
* [http://wiki.mikrotik.com/wiki/Dynamic_DNS_Update_Script_for_dynDNS](http://wiki.mikrotik.com/wiki/Dynamic_DNS_Update_Script_for_dynDNS)